package it.unifi.micc.java_base;

public class BookShop {

	public static void main(String[] args) {
		Book b = new Book("Il vecchio e il mare", "Ernest Hemingway", 75);
		
		Shelf s = new Shelf(100);
		//Esercizio:
		//Modificare la dimensione di Shelf ad un valore basso(per es.: 2) e inserire
		//3 libri. Che succede?
				
		s.addBook(b);
		s.addBook(new Book("Il Signore degli Anelli","JRR Tolkien",1000));
		
		
		//Esercizio, modificare l'istruzione sotto per stampare correttamente il
		//plurale singolare in base al quantitativo di libri (numBooks).
		System.out.println("Ho " + s.getNumBooks() + " libri nel mio scaffale");
		
		//Esercizio implementare Shelf.printall();
		//il metodo stampa tutti i libri dello scaffale
		s.printAll();
		
		//Esercizio:
		//implementare il metodo Shelf.removeBook(int bookId)
		//il metodo rimuove il libro dallo scaffale e riposiziona i libri in modo
		//che occupino le prime posizioni della array.
	}

}
